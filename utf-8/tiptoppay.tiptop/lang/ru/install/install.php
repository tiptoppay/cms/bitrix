<?
$MESS['tiptoppay.tiptop_MODULE_NAME'] = "Платежная система TipTopPay";
$MESS['tiptoppay.tiptop_MODULE_DESC'] = "Интеграция платежной системы TipTopPay";
$MESS['tiptoppay.tiptop_PARTNER_NAME'] = 'tiptoppay';
$MESS["tiptoppay.tiptop_PARTNER_URI"] = 'https://tiptoppay.kz/';
$MESS['UV_INSTALL_TITLE'] = "Установка модуля";
$MESS['UV_UNINSTALL_TITLE'] = "Деинсталяция модуля";
$MESS['tiptoppay_STATUS1']='TipTopPay: Авторизован';
$MESS['tiptoppay_STATUS2']='TipTopPay: Возврат оплаты';
$MESS['tiptoppay_STATUS3']='TipTopPay: Подтверждение оплаты';
$MESS['tiptoppay_STATUS4']='TipTopPay: Отмена авторизованного платежа';
$MESS['EVENT_NAME1']="Отправить счет клиенту";
$MESS['MAIL_SUBJECT']="#SITE_NAME#: Просьба оплатить счет по заказу";
$MESS['MAIL_TEXT1']=
"
----------------------------------- #SITE_NAME#: Зарегистрирован заказ.<br>
Содержимое корзины:<br>
#BASKET_LIST#<br>
Сумма оплаты: #ORDER_SUMM#<br>
Ссылка на оплату: #ORDER_LINK#.";
?>
