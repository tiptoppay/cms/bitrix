<?php
$MESS["SALE_HPS_TIPTOP_CUSTOMER_REJECTION"] = "Отказ клиента";
$MESS["SALE_HPS_TIPTOP_REFUND_ERROR"] = "Во время выполнения возврата перевода возникла ошибка.";
$MESS["SALE_HPS_TIPTOP_REFUND_ERROR_INFO"] = "Статус: #STATUS#. Код ошибки: #ERROR#";
$MESS["SALE_HPS_TIPTOP_REFUND_CONNECTION_ERROR"] = "Ошибка подключения. URL: #URL#. HTTP код: #CODE#. Ошибка: #ERROR#";
$MESS["SALE_HPS_TIPTOP_TRANSACTION"] = "Транзакция";
$MESS["SALE_HPS_TIPTOP_DATE_PAYED"] = "Дата оплаты";
$MESS["DELIVERY"] = "Доставка";