<?
$MESS['PAGE_TITLE']="Страница оплаты заказа";
$MESS['SUCCESS_MESSAGE']="Ваш заказ <b>№ #ORDER_ID#</b> от #DATE# успешно создан.";
$MESS['ORDER_NUMBER']="Номер вашей оплаты: <b>№#ORDER_ID#</b>";
$MESS['PERSONAL_LINK']="Вы можете следить за выполнением своего заказа в <a href='/personal/order/'>Персональном разделе сайта</a>.";
$MESS['WARNING_MESSAGE']="Обратите внимание, что для входа в этот раздел вам необходимо будет ввести логин и пароль пользователя сайта.";
$MESS['WIDGET_DESC']='заказ № #ORDER_ID# на "#SITE_NAME#" от #DATE#';
$MESS['PAY_BUTTON']='Оплатить';
$MESS['WRONG_HASH']="Не верный hash заказа";
$MESS['ORDER_NOT_FOUND']="Заказ не найден";
$MESS['WRONG_AU_STATUS']="Оплата по заказу уже авторизована";
$MESS ['WRONG_ORDER_PAY']="Заказ уже оплачен";
$MESS ['DELIVERY_TXT']='Доставка';
?>