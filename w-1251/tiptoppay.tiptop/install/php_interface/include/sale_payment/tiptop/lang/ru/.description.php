<?php
  $MESS['SALE_HPS_TIPTOP'] = 'TipTopPay';
  $MESS["SALE_HPS_TIPTOP_SHOP_ID"] = "Public ID";
  $MESS["SALE_HPS_TIPTOP_SHOP_ID_DESC"] = "���� ������� (�� ������� �������� TipTopPay)";
  $MESS["SALE_HPS_TIPTOP_PAYMENT_ID"] = "��� ������ (ID)";
  $MESS["SALE_HPS_TIPTOP_PAY_ID"] = "��� ������� (ID)";
  $MESS["CONNECT_SETTINGS_TIPTOP"] = '��������� ��������� �������';
  $MESS["SALE_HPS_TIPTOP_SHOP_KEY"] = "������ ��� API";
  $MESS["SALE_HPS_TIPTOP_SHOP_KEY_DESC"] = "������ ������� (�� ������� �������� TipTopPay)";
  $MESS["SALE_HPS_TIPTOP_CHECKONLINE"] = "������������ ���������� ������ ���� TipTopKassir";
  $MESS["SALE_HPS_TIPTOP_CHECKONLINE_DESC"] = "������ ���������� ������ ���� ������� �� ������� TipTopPay";
  $MESS["SALE_HPS_TIPTOP_SHOULD_PAY"] = "����� � ������";
  $MESS["SALE_HPS_TIPTOP_PAYMENT_DATE"] = "���� �������� ������";
  $MESS["SALE_HPS_TIPTOP_IS_TEST"] = "�������� �����";
  $MESS["SALE_HPS_TIPTOP_CHANGE_STATUS_PAY"] = "������������� ���������� ����� ��� ��������� ��������� ������� ������";
  $MESS["SALE_HPS_TIPTOP_PAYMENT_TYPE"] = "��� �������� �������";
  $MESS["SALE_HPS_TIPTOP_BUYER_ID"] = "��� ����������";
  $MESS["SALE_HPS_TIPTOP_BUYER_EMAIL"] = "Email ����������";
  $MESS["SALE_HPS_TIPTOP_BUYER_PHONE"] = "������� ����������";
  $MESS["SALE_HPS_TIPTOP_CURRENCY"] = "������ ������";
  $MESS["SALE_HPS_TIPTOP_RETURN"] = "�������� �������� �� ��������������";
  $MESS["SALE_HPS_TIPTOP_RESTRICTION"] = "����������� �� ����� �������� ������� �� ������� ������, ������� ������� ����������";
  $MESS["SALE_HPS_TIPTOP_COMMISSION"] = "��� ������� ��� ����������";
  $MESS["SALE_HPS_TIPTOP_INN"] = "���/��� �����������";
  $MESS["SALE_HPS_TIPTOP_INN_DESC"] = "���/��� ����� ����������� ��� ��, �� ������� ���������������� �����";
  $MESS["SALE_HPS_TIPTOP_TYPE_NALOG"] = '��� ������� ���������������';
  $MESS["SALE_HPS_TIPTOP_TYPE_NALOG_DESC"] = '��������� ������� ��������������� ������ ��������� � ����� �� ���������, ������������������ � ���.';
  $MESS["SALE_HPS_NALOG_TYPE_0"] = '����������������� ����� ���������������';
  $MESS["SALE_HPS_NALOG_TYPE_1"] = '��� �� ������ ���������� ����������';
  $MESS["SALE_HPS_NALOG_TYPE_4"] = '��� �� ������ �������';
  $MESS["VBCH_CLPAY_SPTTP_DDESCR"] = "<a href=\"https://tiptoppay.kz/\">TipTopPay</a>.<br>���� �������� ������ � ������� ���������� ����� ����� ������� TipTopPay <Br/>
����� � ������ ������� TipTopPay � ��������� ����: <br/>
&nbsp;&nbsp;	��������� �heck �����������: https://" . $_SERVER['HTTP_HOST'] . "/bitrix/tools/sale_ps_result.php?action=check<br/>
&nbsp;&nbsp;	��������� Pay �����������: https://" . $_SERVER['HTTP_HOST'] . "/bitrix/tools/sale_ps_result.php?action=pay<br/>
&nbsp;&nbsp;	��������� Fail �����������: https://" . $_SERVER['HTTP_HOST'] . "/bitrix/tools/sale_ps_result.php?action=fail<br/>
&nbsp;&nbsp;	��������� Confirm �����������: https://" . $_SERVER['HTTP_HOST'] . "/bitrix/tools/sale_ps_result.php?action=confirm<br/>
&nbsp;&nbsp;	��������� Refund �����������: https://" . $_SERVER['HTTP_HOST'] . "/bitrix/tools/sale_ps_result.php?action=refund<br/>
&nbsp;&nbsp;	��������� �ancel �����������: https://" . $_SERVER['HTTP_HOST'] . "/bitrix/tools/sale_ps_result.php?action=cancel<br/>";

  $MESS["SALE_HPS_TIPTOP_TYPE_SYSTEM"] = "��� ����� ���������� ��������";
  $MESS["SALE_HPS_TYPE_SCHEME_0"] = "������������� ������";
  $MESS["SALE_HPS_TYPE_SCHEME_1"] = "������������� ������";

  $MESS["SALE_HPS_TIPTOP_SUCCESS_URL"] = "Success URL";
  $MESS["SALE_HPS_TIPTOP_SUCCESS_URL_DESC"] = "";
  $MESS["SALE_HPS_TIPTOP_FAIL_URL"] = "Fail URL";
  $MESS["SALE_HPS_TIPTOP_FAIL_URL_DESC"] = "";
  $MESS["SALE_HPS_TIPTOP_WIDGET_LANG"] = "���� �������";
  $MESS["SALE_HPS_TIPTOP_WIDGET_LANG_DESC"] = "";

  $MESS["SALE_HPS_WIDGET_LANG_TYPE_0"] = "������� MSK";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_1"] = "���������� CET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_2"] = "��������� CET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_3"] = "��������������� AZT";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_4"] = "������� ALMT";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_5"] = "��������� ALMT";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_6"] = "���������� EET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_7"] = "�������� CET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_8"] = "������������� CET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_9"] = "������� CET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_10"] = "����������� ICT";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_11"] = "�������� TRT";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_12"] = "��������� CET";
  $MESS["SALE_HPS_WIDGET_LANG_TYPE_13"] = "��������� UZT";



  $MESS["SALE_HPS_TIPTOP_VAT_DELIVERY"] = "�������� ��� �� ��������, ���� ����������";
  $MESS["SALE_HPS_TIPTOP_VAT_DELIVERY_DESC"] = "";

  $MESS["VAT"] = "�������� ��� �� ��������, ���� ����������";
  $MESS["NOT_VAT"] = "��� ���";
  $MESS["DELIVERY_VAT0"]="��� 0%";
  $MESS["DELIVERY_VAT12"]="��� 12%";

  $MESS["STATUS_GROUP"] = "������� ������� ����������� �����";

  $MESS["STATUS_TWOCHECK"] = "������ ��� �������� ���������� ����";
  $MESS["STATUS_GROUP3"] = "��-54";
  $MESS["STATUS_GROUP2"] = '������� � ������� ����������� ����� (�������� ���� ���������� �������� "��������� �������"';
  $MESS["STATUS_PAY"] = "������ �������";
  $MESS["STATUS_CHANCEL"] = "������ �������� �������";
  $MESS["STATUS_AUTHORIZE"] = "������ ������������� ����������� ������� (������������� �������)";
  $MESS["STATUS_AU"] = "������ ��������������� ������� (������������� �������)";
  $MESS["STATUS_VOID"] = "������ ������ ��������������� ������� (������������� �������)";
  $MESS["STATUS_PARTIAL_PAY"] = "������ ��������� ������";

  $MESS["SALE_HPS_TIPTOP_COURSE_RATE"] = "����������� ����� �����-������� �� ��������� � ����� ��";
  $MESS["SALE_HPS_TIPTOP_COURSE_RATE_DESC"] = "";

  $MESS["SALE_HPS_TIPTOP_NEW_STATUS"] = "��������� �������";
  $MESS["SALE_HPS_TIPTOP_NEW_STATUS_DESC"] = "���������� ���������� ���������� ������������ �������� � �������� ��� �����������";

  $MESS["SALE_HPS_TIPTOP_WIDGET_DESIGN"] = "������ �������";
  $MESS["SALE_HPS_TIPTOP_WIDGET_DESIGN_DESC"] = "";
  $MESS["SALE_HPS_WIDGET_DESIGN_TYPE_0"] = "classic";
  $MESS["SALE_HPS_WIDGET_DESIGN_TYPE_1"] = "modern";
  $MESS["SALE_HPS_WIDGET_DESIGN_TYPE_2"] = "mini";

  $MESS["SALE_HPS_TIPTOP_CalculationPlace"] = "����� ������������� ��������";
  $MESS["SALE_HPS_TIPTOP_CalculationPlace_DESC"] = "����� (������) ����� ����� ������, ��� ������ ����";

//  $MESS["SALE_HPS_TIPTOP_SPIC"] = "��� ���� ��������";
//  $MESS["SALE_HPS_TIPTOP_PACKAGE_CODE"] = "��� �������� ��������";

  $MESS["CREATE_ORDER1"] = "�������� ��� �������� �����";
  $MESS["CREATE_ORDER_EMAIL"] = "��� �������� �����";

  $MESS["CREATE_ORDER_PHONE"] = "��������� �� SMS";
  $MESS["CREATE_ORDER_PHONE_DESC"] = "�������� ������ � ������� TipTopPay";
  
  $MESS["CREATE_ORDER_EMAIL"] = "��������� �� Email";

  $MESS["OLD_TYPE"] = "�������� ������ �� ������ �� �����";
  $MESS["NEW_TYPE"] = "�������� ������� tiptoppay";
  $MESS["CREATE_ORDER_TYPE"] = "��� �������� �����";

  $MESS["SPOSOB_RASCHETA1"]="������� ������� �������";
  $MESS["PREDMET_RASCHETA1"]="������� �������� �������";


  $MESS["SPOSOB_RASCHETA1_0"] = "����������� ������ �������";
  $MESS["SPOSOB_RASCHETA1_1"] = "���������� 100%";
  $MESS["SPOSOB_RASCHETA1_2"] = "����������";
  $MESS["SPOSOB_RASCHETA1_3"] = "�����";
  $MESS["SPOSOB_RASCHETA1_4"] = "������ ������";

  $MESS["PREDMET_RASCHETA1_0"] = "����������� ������� ������";
  $MESS["PREDMET_RASCHETA1_1"] = "�����";
  $MESS["PREDMET_RASCHETA1_2"] = "����������� �����";
  $MESS["PREDMET_RASCHETA1_3"] = "������";
  $MESS["PREDMET_RASCHETA1_4"] = "������";