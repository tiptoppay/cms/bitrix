<?php
	IncludeModuleLangFile(__DIR__ . "/install.php");
	
	if(class_exists("tiptoppay_tiptop")) return;
	
	class tiptoppay_tiptop extends CModule
	{
		var $MODULE_ID = "tiptoppay.tiptop";
		var $MODULE_VERSION;
		var $MODULE_VERSION_DATE;
		var $MODULE_NAME;
		var $MODULE_DESCRIPTION;
		var $MODULE_GROUP_RIGHTS = "Y";
		function __construct()
		{
			$arModuleVersion = array();
			include(dirname(__FILE__)."/version.php");
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
			$this->MODULE_NAME = GetMessage("tiptoppay.tiptop_MODULE_NAME");
			$this->MODULE_DESCRIPTION = GetMessage("tiptoppay.tiptop_MODULE_DESC");
			$this->PARTNER_NAME = GetMessage("tiptoppay.tiptop_PARTNER_NAME");
			$this->PARTNER_URI = GetMessage("tiptoppay.tiptop_PARTNER_URI");
		}
		
		function DoInstall()
		{
			if (IsModuleInstalled($this->MODULE_ID)) {
				$this->UnInstallFiles();
			}
			
			$this->InstallFiles();
			RegisterModule($this->MODULE_ID);
			$this->InstallEvents();
			$this->InstallOrderStatus();
			return true;
		}
		
		function InstallFiles()
		{
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/images", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images",true,true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/php_interface",  $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface",true,true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/admin",  $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin",true,true);
			mkdir($_SERVER["DOCUMENT_ROOT"]."/tiptopPay");
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/front",  $_SERVER["DOCUMENT_ROOT"]."/tiptopPay",true,true);
			return true;
		}
		
		function UnInstallFiles()
		{
			DeleteDirFilesEx('/bitrix/php_interface/include/sale_payment/tiptop');
			unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/tiptop_adminbutton.php");
			unlink($_SERVER["DOCUMENT_ROOT"]."/tiptopPay/pay.php");
			unlink($_SERVER["DOCUMENT_ROOT"]."/tiptopPay/lang/ru/pay.php");
			rmdir($_SERVER["DOCUMENT_ROOT"].'/tiptopPay/lang/ru');
			rmdir($_SERVER["DOCUMENT_ROOT"].'/tiptopPay/lang');
			rmdir($_SERVER["DOCUMENT_ROOT"].'/tiptopPay');
			return true;
		}
		
		function InstallOrderStatus()
		{
			if (!CModule::IncludeModule("sale"))
				return false;
			
			CSaleStatus::Add(array(
				'ID' => 'AU',
				'SORT' => 998,
				'LANG' =>array(
					array("LID"=>'ru',"NAME"=>GetMessage("tiptoppay_STATUS1")),
					array("LID"=>'en',"NAME"=>"TipTopPay: Authorized")
				),
			));
			
			CSaleStatus::Add(array(
				'ID' => 'RR',
				'SORT' => 1000,
				'LANG' =>array(
					array("LID"=>'ru',"NAME"=>GetMessage("tiptoppay_STATUS2")),
					array("LID"=>'en',"NAME"=>"TipTopPay: Refund")
				),
			));
			
			CSaleStatus::Add(array(
				'ID' => 'TTP',
				'SORT' => 999,
				'LANG' =>array(
					array("LID"=>'ru',"NAME"=>GetMessage("tiptoppay_STATUS3")),
					array("LID"=>'en',"NAME"=>"TipTopPay: Confirm")
				),
			));
			
			CSaleStatus::Add(array(
				'ID' => 'AR',
				'SORT' => 1001,
				'LANG' =>array(
					array("LID"=>'ru',"NAME"=>GetMessage("tiptoppay_STATUS4")),
					array("LID"=>'en',"NAME"=>"TipTopPay: Void")
				),
			));
		}
		
		function UnInstallOrderStatus()
		{
			if (!CModule::IncludeModule("sale"))
				return false;
			
			CSaleStatus::Delete("AU");
			CSaleStatus::Delete("RR");
			CSaleStatus::Delete("TTP");
			CSaleStatus::Delete("AR");
		}
		
		
		function InstallEvents()
		{
			$obEventType = new CEventType;
			$obEventType->Add(array(
				"EVENT_NAME"    => "SEND_BILL",
				"NAME"          => GetMessage("EVENT_NAME1"),
				"LID"           => "ru",
				"DESCRIPTION"   => ""
			));
			
			$obTemplate = new CEventMessage;
			$obTemplate->Add(array(
				"ACTIVE"      => "Y",
				"EVENT_NAME"  => "SEND_BILL",
				"LID"         => array("s1"),
				"EMAIL_FROM"  => "#DEFAULT_EMAIL_FROM#",
				"EMAIL_TO"    => "#EMAIL_TO#",
				"BCC"         => "",
				"SUBJECT"     => GetMessage("MAIL_SUBJECT"),
				"BODY_TYPE"   => "html",
				"MESSAGE"     =>GetMessage("MAIL_TEXT1")
			));
			
			$eventManager = \Bitrix\Main\EventManager::getInstance();
			$eventManager->registerEventHandler("main", "OnAdminContextMenuShow", $this->MODULE_ID, "TiptopHandler2", "OnAdminContextMenuShowHandler_button",9999);
			$eventManager->registerEventHandler("sale", "OnBeforeOrderDelete", $this->MODULE_ID, "TiptopHandler2", "OnTiptopOrderDelete",9999);
			$eventManager->registerEventHandler("sale", "OnSaleStatusOrder", $this->MODULE_ID, "TiptopHandler2", "OnTiptopStatusUpdate",9999);
			$eventManager->registerEventHandler("sale", "OnSaleBeforeCancelOrder", $this->MODULE_ID, "TiptopHandler2", "OnTiptopOnSaleBeforeCancelOrder",9999);
			
			return true;
		}
		
		function UnInstallEvents()
		{
			$eventManager = \Bitrix\Main\EventManager::getInstance();
			$eventManager->unRegisterEventHandler("main", "OnAdminContextMenuShow", $this->MODULE_ID, "TiptopHandler2", "OnAdminContextMenuShowHandler_button");
			$eventManager->unRegisterEventHandler("sale", "OnBeforeOrderDelete", $this->MODULE_ID, "TiptopHandler2", "OnTiptopOrderDelete");
			$eventManager->unRegisterEventHandler("sale", "OnSaleStatusOrder", $this->MODULE_ID, "TiptopHandler2", "OnTiptopStatusUpdate");
			$eventManager->unRegisterEventHandler("sale", "OnSaleBeforeCancelOrder", $this->MODULE_ID, "TiptopHandler2", "OnTiptopOnSaleBeforeCancelOrder");
			$emessage = new CEventMessage;
			
			$rsMess = CEventMessage::GetList("site_id", "desc", array("TYPE_ID" => array("SEND_BILL")));
			
			while($arMess = $rsMess->GetNext()) { $emessage->Delete($arMess['ID']); }
			
			$et = new CEventType;
			$et->Delete("SEND_BILL");
			return true;
		}
		
		function DoUninstall()
		{
			$this->UnInstallEvents();
			$this->UnInstallFiles();
			$this->UnInstallOrderStatus();
			UnRegisterModule($this->MODULE_ID);
			
			return true;
		}
	}